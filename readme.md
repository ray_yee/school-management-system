## Project Name & Pitch

School Management System

An application used to manage the day to day operations at a sports academy.

## Project Status

This project is currently being retired as the technology used is outdated.

## Project Screen Shot(s)

![Screenshot](public/images/screenshot.jpg?raw=true 'Title')

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.

Installation:

`npm install`

To Start Server:

`npm start`

To Visit App:

`localhost:8080`
